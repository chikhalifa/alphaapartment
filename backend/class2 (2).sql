-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 27, 2019 at 02:32 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `class2`
--

-- --------------------------------------------------------

--
-- Table structure for table `alpha2`
--

CREATE TABLE `alpha2` (
  `id` int(11) NOT NULL,
  `firstname` varchar(25) NOT NULL,
  `lastname` varchar(25) NOT NULL,
  `email` varchar(25) NOT NULL,
  `psw_1` varchar(25) NOT NULL,
  `psw_2` varchar(25) NOT NULL,
  `Address` varchar(25) NOT NULL,
  `trn_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alpha2`
--

INSERT INTO `alpha2` (`id`, `firstname`, `lastname`, `email`, `psw_1`, `psw_2`, `Address`, `trn_date`) VALUES
(1, 'sidney', 'wilson', '81dc9bdb52d04dc20036dbd83', '81dc9bdb52d04dc20036dbd83', 'sidney@gmail.com', 'jjadjfa ', '2018-06-21 16:22:15'),
(2, 'obafemi', 'jalkdjf', '99c5e07b4d5de9d18c350cdf6', '99c5e07b4d5de9d18c350cdf6', 'metiehesther@gmail.com', '', '2018-06-21 16:22:51'),
(3, 'Esther', 'Metieh', 'f962bed5616612c8c7053f6e9', 'f962bed5616612c8c7053f6e9', 'metiehesther@gmail.com', 'ojktttttttttttttttttttttt', '2018-06-21 16:25:46'),
(4, 'chika', 'joseph', '2b016d90959eda144d600e4f8', '2b016d90959eda144d600e4f8', 'chikaj35@yahoo.com', 'ueuhfieojgk', '2018-06-21 16:42:44'),
(5, 'chinyere', 'maria', '73d238a186c17d580c4901645', '73d238a186c17d580c4901645', 'ekeanyanwuchiyre@yahoo.co', 'fhfhj', '2018-06-21 16:54:39'),
(6, 'obafemi', 'wilson', 'b59c67bf196a4758191e42f76', 'b59c67bf196a4758191e42f76', 'chikaj35@yahoo.com', 'ssss', '2018-06-21 17:30:56'),
(7, 'Esther', 'jalkdjf', 'b59c67bf196a4758191e42f76', '934b535800b1cba8f96a5d72f', 'chikaj35@yahoo.com', '', '2018-06-21 17:51:31'),
(8, 'Esther', 'jalkdjf', 'b59c67bf196a4758191e42f76', '934b535800b1cba8f96a5d72f', 'chikaj35@yahoo.com', '', '2018-06-21 22:02:06'),
(9, 'chika', 'wilson', 'b59c67bf196a4758191e42f76', 'b59c67bf196a4758191e42f76', 'chikaj35@yahoo.com', 'sssssssssssssssssssssss', '2018-06-21 22:02:50'),
(10, 'chika', 'wilson', '37d153a06c79e99e4de5889db', 'b59c67bf196a4758191e42f76', 'Odyree', 'ffff', '2018-06-21 22:15:02'),
(11, '', '', '37d153a06c79e99e4de5889db', 'd41d8cd98f00b204e9800998e', 'Odyree', '', '2018-06-21 22:29:45'),
(12, '', '', 'd41d8cd98f00b204e9800998e', 'd41d8cd98f00b204e9800998e', '', '', '2018-06-21 22:30:42'),
(13, '', '', '0cc175b9c0f1b6a831c399e26', 'd41d8cd98f00b204e9800998e', '', '', '2018-06-21 22:36:33'),
(14, '', '', 'd41d8cd98f00b204e9800998e', 'd41d8cd98f00b204e9800998e', '', '', '2018-06-21 22:39:52'),
(15, '', '', 'd41d8cd98f00b204e9800998e', 'd41d8cd98f00b204e9800998e', '', '', '2018-06-21 22:39:59'),
(16, 'obafemi', 'wilson', '0cc175b9c0f1b6a831c399e26', 'c4ca4238a0b923820dcc509a6', 'car', '', '2018-06-22 10:10:50'),
(17, 'obafemi', 'joseph', '0cc175b9c0f1b6a831c399e26', 'c4ca4238a0b923820dcc509a6', 'car', '', '2018-06-22 10:31:18'),
(18, 'emma', 'zac', '81dc9bdb52d04dc20036dbd83', '81dc9bdb52d04dc20036dbd83', 'mum@yahoo.com', 'fhjkkjffjffj', '2018-06-22 10:37:18'),
(19, 'obafemi', 'wilson', '202cb962ac59075b964b07152', '202cb962ac59075b964b07152', 'chikaj35@yahoo.com', 'ssssssssssssssss', '2018-06-22 11:53:05'),
(20, 'sidney', 'wilson', '81dc9bdb52d04dc20036dbd83', '81dc9bdb52d04dc20036dbd83', 'chikaj35@yahoo.com', 'fffffffffffffffffffffffff', '2018-06-22 11:54:26'),
(21, 'dhayo', 'dayo', 'dhayo6@yahoo.com', '36d4c3fc73e28bd545ad5c274', '36d4c3fc73e28bd545ad5c274', '1, white house street', '2018-06-22 12:02:43'),
(22, 'obafemi', 'zac', 'chikaj35@yahoo.com', 'b59c67bf196a4758191e42f76', 'b59c67bf196a4758191e42f76', 'ddddddd', '2018-06-22 12:10:38'),
(23, 'obafemi', 'joseph', '37d153a06c79e99e4de5889db', 'b0baee9d279d34fa1dfd71aad', 'metiehesther@gmail.com', 'qqqqqqqqqqqqqqq', '2018-06-22 12:49:08'),
(24, 'emma', 'zac', 'bcbe3365e6ac95ea2c0343a23', 'bcbe3365e6ac95ea2c0343a23', 'chikaj35@yahoo.com', 'fffffffff', '2018-06-22 13:02:01'),
(25, 'obafemi', 'jalkdjf', 'b59c67bf196a4758191e42f76', 'b59c67bf196a4758191e42f76', 'chikaj35@yahoo.com', 'xfffffffffffffffffffff', '2018-06-22 13:07:50'),
(26, 'obafemi', 'jalkdjf', 'b59c67bf196a4758191e42f76', '41fcba09f2bdcdf315ba4119d', 'chikaj35@yahoo.com', 'ddddddddddddddddddd', '2018-06-22 13:09:25'),
(27, 'chika', 'joseph', 'chikaj35@yahoo.com', 'b59c67bf196a4758191e42f76', 'b59c67bf196a4758191e42f76', '333333333', '2018-06-22 13:16:18'),
(28, 'obafemi', 'joseph', 'b59c67bf196a4758191e42f76', '74b87337454200d4d33f80c46', 'chikaj35@yahoo.com', '', '2018-06-22 13:19:50'),
(29, 'obafemi', 'maria', 'chikaj35@yahoo.com', '6512bd43d9caa6e02c990b0a8', '6512bd43d9caa6e02c990b0a8', 'gggggggggggggg', '2018-06-22 13:32:45'),
(30, 'sidney', 'jalkdjf', 'chikaj35@yahoo.com', 'b59c67bf196a4758191e42f76', '4786f3282f04de5b5c7317c49', 'vvvvv', '2018-06-22 14:22:07'),
(31, 'emma', 'zac', 'metiehesther@gmail.com', 'b2ca678b4c936f905fb82f273', 'b2ca678b4c936f905fb82f273', 'vvvvvvrrr', '2018-06-27 20:24:23'),
(32, 'emma', 'zac', 'metiehesther@gmail.com', 'b2ca678b4c936f905fb82f273', 'b2ca678b4c936f905fb82f273', 'vvvvvvrrr', '2018-06-27 20:43:16'),
(33, 'bruce', 'wayne', 'jr@gmail.com', '698d51a19d8a121ce581499d7', 'b59c67bf196a4758191e42f76', 'wayne minor', '2018-06-27 20:44:15'),
(34, 'obafemi', 'wilson', 'metiehesther@gmail.com', '4eae35f1b35977a00ebd8086c', '4eae35f1b35977a00ebd8086c', 'dffffffffffffffffffffffff', '2018-06-27 21:14:42'),
(35, 'obafemi', 'wilson', 'metiehesther@gmail.com', '4eae35f1b35977a00ebd8086c', '4eae35f1b35977a00ebd8086c', 'dffffffffffffffffffffffff', '2018-06-27 21:16:42'),
(36, 'obafemi', 'joseph', 'chikaj35@yahoo.com', '698d51a19d8a121ce581499d7', '202cb962ac59075b964b07152', 'www', '2018-07-10 21:33:45'),
(37, 'obafemi', 'joseph', 'chikaj35@yahoo.com', '698d51a19d8a121ce581499d7', '202cb962ac59075b964b07152', 'www', '2018-07-10 21:34:52'),
(38, 'obafemi', 'joseph', 'chikaj35@yahoo.com', '698d51a19d8a121ce581499d7', '202cb962ac59075b964b07152', 'www', '2018-07-10 21:35:56'),
(39, 'obafemi', 'joseph', 'chikaj35@yahoo.com', '698d51a19d8a121ce581499d7', '202cb962ac59075b964b07152', 'www', '2018-07-10 21:36:00'),
(40, 'obafemi', 'joseph', 'chikaj35@yahoo.com', '698d51a19d8a121ce581499d7', '202cb962ac59075b964b07152', 'www', '2018-07-10 21:42:37'),
(41, 'obafemi', 'joseph', 'chikaj35@yahoo.com', '698d51a19d8a121ce581499d7', '202cb962ac59075b964b07152', 'www', '2018-07-10 21:46:15'),
(42, 'obafemi', 'joseph', 'chikaj35@yahoo.com', '698d51a19d8a121ce581499d7', '202cb962ac59075b964b07152', 'www', '2018-07-10 21:47:53'),
(43, 'obafemi', 'joseph', 'chikaj35@yahoo.com', '698d51a19d8a121ce581499d7', '202cb962ac59075b964b07152', 'www', '2018-07-10 21:51:46'),
(44, 'obafemi', 'joseph', 'chikaj35@yahoo.com', '698d51a19d8a121ce581499d7', '202cb962ac59075b964b07152', 'www', '2018-07-10 21:54:22'),
(45, 'obafemi', 'joseph', 'chikaj35@yahoo.com', '698d51a19d8a121ce581499d7', '202cb962ac59075b964b07152', 'www', '2018-07-10 21:56:34'),
(46, 'obafemi', 'joseph', 'chikaj35@yahoo.com', '698d51a19d8a121ce581499d7', '202cb962ac59075b964b07152', 'www', '2018-07-10 21:58:56'),
(47, 'obafemi', 'joseph', 'chikaj35@yahoo.com', '698d51a19d8a121ce581499d7', '202cb962ac59075b964b07152', 'www', '2018-07-10 21:59:50'),
(48, 'obafemi', 'joseph', 'chikaj35@yahoo.com', '698d51a19d8a121ce581499d7', '202cb962ac59075b964b07152', 'www', '2018-07-10 22:01:37'),
(49, 'obafemi', 'joseph', 'chikaj35@yahoo.com', '698d51a19d8a121ce581499d7', '202cb962ac59075b964b07152', 'www', '2018-07-10 22:01:57'),
(50, 'obafemi', 'joseph', 'chikaj35@yahoo.com', '698d51a19d8a121ce581499d7', '202cb962ac59075b964b07152', 'www', '2018-07-10 22:40:09'),
(51, 'obafemi', 'joseph', 'chikaj35@yahoo.com', '698d51a19d8a121ce581499d7', '202cb962ac59075b964b07152', 'www', '2018-07-10 22:41:09'),
(52, 'obafemi', 'joseph', 'chikaj35@yahoo.com', '698d51a19d8a121ce581499d7', '202cb962ac59075b964b07152', 'www', '2018-07-10 22:42:12'),
(53, 'obafemi', 'joseph', 'chikaj35@yahoo.com', '698d51a19d8a121ce581499d7', '202cb962ac59075b964b07152', 'www', '2018-07-10 22:43:02'),
(54, 'obafemi', 'joseph', 'chikaj35@yahoo.com', '698d51a19d8a121ce581499d7', '202cb962ac59075b964b07152', 'www', '2018-07-10 22:44:35'),
(55, 'obafemi', 'joseph', 'chikaj35@yahoo.com', '698d51a19d8a121ce581499d7', '202cb962ac59075b964b07152', 'www', '2018-07-10 22:47:17'),
(56, 'obafemi', 'joseph', 'chikaj35@yahoo.com', '698d51a19d8a121ce581499d7', '202cb962ac59075b964b07152', 'www', '2018-07-10 22:47:46'),
(57, 'obafemi', 'joseph', 'chikaj35@yahoo.com', '698d51a19d8a121ce581499d7', '202cb962ac59075b964b07152', 'www', '2018-07-10 22:57:15'),
(58, 'obafemi', 'joseph', 'chikaj35@yahoo.com', '698d51a19d8a121ce581499d7', '202cb962ac59075b964b07152', 'www', '2018-07-10 23:03:42'),
(59, 'obafemi', 'joseph', 'chikaj35@yahoo.com', '698d51a19d8a121ce581499d7', '202cb962ac59075b964b07152', 'www', '2018-07-11 01:44:05'),
(60, 'emma', 'zac', 'chikaj35@yahoo.com', '698d51a19d8a121ce581499d7', '698d51a19d8a121ce581499d7', 'rrrrrrrrrrrrrrrrr', '2018-07-11 02:04:32'),
(61, 'obafemi', 'maria', 'metiehesther@gmail.com', 'b59c67bf196a4758191e42f76', 'b59c67bf196a4758191e42f76', '', '2018-07-11 02:36:34'),
(62, 'obafemi', 'jalkdjf', 'metiehesther@gmail.com', '9df62e693988eb4e1e1444ece', '0c744b578002c7fb9e70e25b4', '', '2018-07-11 02:48:49'),
(63, 'emma', 'Metieh', 'metiehesther@gmail.com', 'b2ca678b4c936f905fb82f273', 'b2ca678b4c936f905fb82f273', '', '2018-07-11 02:51:50'),
(64, 'chinyere', 'zac', 'metiehesther@gmail.com', '4a8a08f09d37b737956490384', '4a8a08f09d37b737956490384', '', '2018-07-11 03:36:44'),
(65, 'ig', 'Metieh', 'ignatius@yahoo.com', '827ccb0eea8a706c4c34a1689', '827ccb0eea8a706c4c34a1689', 'hkglugulut', '2018-07-16 20:48:44'),
(66, 'obafemi', 'maria', 'metiehesther@gmail.com', 'bcbe3365e6ac95ea2c0343a23', 'bcbe3365e6ac95ea2c0343a23', 'fbbbbbbbbbbbbbb', '2018-07-20 22:12:07'),
(67, 'chika', 'p', 'metiehesther@gmail.com', '9990775155c3518a0d7917f77', '9990775155c3518a0d7917f77', 'ffff', '2018-07-20 22:14:29'),
(68, 'obafemi', 'joseph', 'metiehesther@gmail.com', 'b59c67bf196a4758191e42f76', 'ad57484016654da87125db86f', 'ffffffffffff', '2018-07-20 22:24:11'),
(69, 'obafemi', 'joseph', 'metiehesther@gmail.com', '182be0c5cdcd5072bb1864cde', '1aabac6d068eef6a7bad3fdf5', 'ffffffffffff', '2018-07-20 22:26:33'),
(70, 'obafemi', 'joseph', 'metiehesther@gmail.com', '4eae35f1b35977a00ebd8086c', '4eae35f1b35977a00ebd8086c', 'ffff', '2018-07-21 17:37:24'),
(71, 'obafemi', 'jalkdjf', 'chikaj35@yahoo.com', '698d51a19d8a121ce581499d7', '698d51a19d8a121ce581499d7', 'vggggg', '2018-07-23 20:21:19'),
(72, 'obafemi', 'joseph', 'chikaj35@yahoo.com', '698d51a19d8a121ce581499d7', '698d51a19d8a121ce581499d7', '', '0000-00-00 00:00:00'),
(73, 'obafemi', 'joseph', 'chikaj35@yahoo.com', '698d51a19d8a121ce581499d7', '698d51a19d8a121ce581499d7', '', '0000-00-00 00:00:00'),
(74, 'obafemi', 'joseph', 'chikaj35@yahoo.com', '698d51a19d8a121ce581499d7', '698d51a19d8a121ce581499d7', '', '0000-00-00 00:00:00'),
(75, 'obafemi', 'joseph', 'chikaj35@yahoo.com', '77963b7a931377ad4ab5ad6a9', '77963b7a931377ad4ab5ad6a9', '', '2018-07-26 06:11:33'),
(76, 'obafemi', 'joseph', 'chikaj35@yahoo.com', '6512bd43d9caa6e02c990b0a8', '6512bd43d9caa6e02c990b0a8', '', '2018-07-28 03:35:40'),
(77, 'chinyere', 'wilson', 'nn@yahoo.com', '099b3b060154898840f0ebdfb', '099b3b060154898840f0ebdfb', 'fffff', '2018-07-28 03:52:29'),
(78, 'nel', 'wilson', 'www@gmail.com', 'b6d767d2f8ed5d21a44b0e588', 'b6d767d2f8ed5d21a44b0e588', 'ffff', '2018-07-28 03:54:38'),
(79, 'obafemi', 'joseph', 'chikaj35@yahoo.com', '6512bd43d9caa6e02c990b0a8', '6512bd43d9caa6e02c990b0a8', 'ff', '2018-07-28 04:02:32'),
(80, 'obafemi', 'joseph', 'chikaj35@yahoo.com', 'b6d767d2f8ed5d21a44b0e588', 'b6d767d2f8ed5d21a44b0e588', 'ff', '2018-07-28 04:03:19'),
(81, 'obafemi', 'joseph', 'ekeanyanwuchiyre@yahoo.co', 'bcbe3365e6ac95ea2c0343a23', 'bcbe3365e6ac95ea2c0343a23', 'ff', '2018-07-28 04:03:44'),
(82, 'obafemi', 'joseph', 'd@yahh', 'b6d767d2f8ed5d21a44b0e588', 'b6d767d2f8ed5d21a44b0e588', 'we are here to rue you pe', '2018-07-28 04:20:31'),
(83, 'chika', 'joseph', 'ch@yahoo.com', 'b59c67bf196a4758191e42f76', 'b59c67bf196a4758191e42f76', 'africa waanda', '2018-08-02 07:37:37'),
(84, 'obafemi', 'joseph', 'CHIKA@YAHOO.COM', '3d2172418ce305c7d16d4b055', '3d2172418ce305c7d16d4b055', '', '2018-08-04 18:11:32'),
(85, 'obafemi', 'joseph', 'caryou', '9f6e6800cfae7749eb6c48661', '9f6e6800cfae7749eb6c48661', '', '2018-08-07 21:06:45'),
(86, 'obafemi', 'joseph', 'me@yaa', '3bad6af0fa4b8b330d162e199', '3bad6af0fa4b8b330d162e199', '', '2018-08-09 13:07:34'),
(87, 'obafemi', 'joseph', 'n@yahoo.com', '47bce5c74f589f4867dbd57e9', '47bce5c74f589f4867dbd57e9', '', '2018-08-09 13:29:30'),
(88, 'obafemi', 'joseph', 'na@yahoo.com', 'b59c67bf196a4758191e42f76', 'b59c67bf196a4758191e42f76', '', '2018-08-09 13:36:48'),
(89, 'obafemi', 'joseph', 'chikaj3@yahoo.com', '77963b7a931377ad4ab5ad6a9', '77963b7a931377ad4ab5ad6a9', '', '2018-08-09 13:37:44'),
(90, 'obafemi', 'joseph', '', '4eae35f1b35977a00ebd8086c', '4eae35f1b35977a00ebd8086c', '', '2018-08-17 20:42:39'),
(91, 'obafemi', 'joseph', 'femi', '4eae35f1b35977a00ebd8086c', '4eae35f1b35977a00ebd8086c', '', '2018-08-17 20:45:40'),
(92, 'obafemi', 'joseph', '35@yahoo.com', 'b2ca678b4c936f905fb82f273', 'b2ca678b4c936f905fb82f273', '', '2018-08-17 20:49:36'),
(93, 'firstname', 'lastname', 'chikhalifa23@yahoo.com', 'df483402b9bfeb234717a32c6', 'df483402b9bfeb234717a32c6', '', '2018-09-02 03:44:26'),
(94, 'AYO', 'JOSHUA', 'AYOOLALEKANJSHUA@GMAIL.CO', '29cd3cccd87a27c71d9f389ad', '29cd3cccd87a27c71d9f389ad', '', '2019-02-06 13:53:33'),
(95, 'nelson', 'motor', 'chi@yahoo.com', '2d00f43f07911355d4151f139', '2d00f43f07911355d4151f139', 'wwwwww', '2019-03-25 23:05:15');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `cat_id` int(8) NOT NULL,
  `cat_name` varchar(255) NOT NULL,
  `cat_description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `contactnumber` varchar(50) NOT NULL,
  `message` varchar(50) NOT NULL,
  `trn_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `contact2`
--

CREATE TABLE `contact2` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `contactnumber` varchar(50) NOT NULL,
  `message` text NOT NULL,
  `trn_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact2`
--

INSERT INTO `contact2` (`id`, `name`, `email`, `contactnumber`, `message`, `trn_date`) VALUES
(1, 'esther metieh', 'chikaj35@yahoo.com', '7777777777777', 'dddddddddddddd', '2018-08-03 22:52:26'),
(2, 'esther metieh', 'chikaj35@yahoo.com', '7777777777777', 'ssssssssss', '2018-08-03 22:53:01'),
(3, 'esther metieh', 'chikaj35@yahoo.com', '7777777777777', 'avengers tower', '2018-08-03 22:54:31'),
(4, 'esther metieh', 'chikaj35@yahoo.com', '7777777777777', 'ssssssssssssssssssssssssssssss', '2018-08-04 17:45:35'),
(5, 'esther metieh', 'chikaj35@yahoo.com', '7777777777777', 'ssssssssssssss', '2018-08-04 19:18:49'),
(6, 'esther metieh', 'chikaj35@yahoo.com', '7777777777777', 'gggggggggggggggggggggggggggggggggggggggggggggggggg', '2018-08-05 23:09:24'),
(7, 'esther metieh', 'chikaj35@yahoo.com', '7777777777777', 'ssssssssssssssssssssss', '2018-08-08 19:42:33'),
(8, 'esther metieh', 'chikaj35@yahoo.com', '7777777777777', 'FFFF', '2018-08-09 12:45:00'),
(9, 'esther metieh', 'chikaj35@yahoo.com', '7777777777777', 'wwwwwwwwwwwwwwwwwwwwwwwwww', '2018-08-10 22:46:12'),
(10, 'esther metieh', 'chikaj35@yahoo.com', '7777777777777', 'WE ARE', '2018-08-11 23:11:25'),
(11, 'esther metieh', 'chikaj35@yahoo.com', '7777777777777', 'fffffffffffffffff', '2018-08-27 15:48:30'),
(12, 'Chika Joseph', 'chikhalifa23@yahoo.com', '22222222222222222', 'ffffffffffffffffffffffffffffffffffffff', '2018-09-07 07:49:22'),
(13, 'Chika Joseph', 'chikhalifa23@yahoo.com', '22222222222222222', 'dddddddddd', '2018-09-14 15:48:44'),
(14, 'Chika Joseph', 'chikhalifa23@yahoo.com', '22222222222222222', 'dddddddddd', '2018-09-14 15:48:44'),
(15, 'esther metieh', 'chikaj35@yahoo.com', '7777777777777', 'wwwwwwwwwwwwwww', '2018-09-23 10:11:41'),
(16, 'esther metieh', 'chikaj35@yahoo.com', '7777777777777', 'we ', '2019-01-20 20:57:57'),
(17, 'chika', 'chikaj35@yahoo.com', '08162387160', 'we are here to make enquires on y', '2019-03-05 11:48:11'),
(18, 'esther metieh', 'metiehesther@gmail.com', '08162387160', 'sfdghjkl;\'', '2019-03-26 12:43:08'),
(19, 'esther metieh', 'metiehesther@gmail.com', '08162387160', 'eeeeeeeeeeeeeeee', '2019-03-27 14:07:05');

-- --------------------------------------------------------

--
-- Table structure for table `creditcard`
--

CREATE TABLE `creditcard` (
  `id` int(11) NOT NULL,
  `card_number` varchar(45) NOT NULL,
  `Month` varchar(45) NOT NULL,
  `Year` varchar(45) NOT NULL,
  `CVV` varchar(45) NOT NULL,
  `trn_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `creditcard`
--

INSERT INTO `creditcard` (`id`, `card_number`, `Month`, `Year`, `CVV`, `trn_date`) VALUES
(1, '88888888888', 'january', '2016', '990', '2018-07-15 20:15:08'),
(2, '88888888888', 'january', '2016', '990', '2018-07-15 20:15:31'),
(3, '11111111111111111111111', 'january', '2022', '2222', '2018-07-15 20:15:59'),
(4, '000000000000000000000000000', 'february', '2017', '346', '2018-07-15 20:16:35'),
(5, '9965953', 'april', '2020', '100', '2018-07-16 02:55:21'),
(6, '88888888888', 'september', '2021', '346', '2018-07-16 02:56:30'),
(7, '000000000000000000000000000', 'may', '2020', '990', '2018-07-16 02:56:57'),
(8, '000000000000000000000000000', 'may', '2020', '990', '2018-07-16 03:05:42'),
(9, '000000000000000000000000000', 'may', '2020', '990', '2018-07-16 03:07:59'),
(10, '000000000000000000000000000', 'may', '2020', '990', '2018-07-16 03:08:59'),
(11, '000000000000000000000000000', 'may', '2020', '990', '2018-07-16 03:10:39'),
(12, '000000000000000000000000000', 'may', '2020', '990', '2018-07-16 03:12:18'),
(13, '000000000000000000000000000', 'may', '2020', '990', '2018-07-16 03:12:40'),
(14, '57777779000999', 'january', '2024', '346', '2018-07-16 03:14:36'),
(15, '57777779000999', 'january', '2024', '346', '2018-07-16 03:16:59'),
(16, '57777779000999', 'january', '2024', '346', '2018-07-16 03:17:04'),
(17, '57777779000999', 'january', '2024', '346', '2018-07-16 03:17:08'),
(18, '57777779000999', 'january', '2024', '346', '2018-07-16 03:17:38'),
(19, '57777779000999', 'january', '2024', '346', '2018-07-16 03:18:16'),
(20, '57777779000999', 'january', '2024', '346', '2018-07-16 03:19:39'),
(21, '57777779000999', 'january', '2024', '346', '2018-07-16 03:20:22'),
(22, '11111111111111111111111', 'january', '2018', '346', '2018-07-16 17:52:36'),
(23, '000000000000000000000000000', 'april', '2019', '346', '2018-07-16 17:53:00'),
(24, '11111111111111111111111', 'january', '2018', '346', '2018-07-26 21:14:47'),
(25, '11111111111111111111111', 'january', '2018', '346', '2018-07-26 22:03:30'),
(26, '11111111111111111111111', 'january', '2018', '346', '2018-07-26 22:03:35'),
(27, '11111111111111111111111', 'january', '2016', '', '2018-07-26 22:04:37'),
(28, '9965953', 'february', '2018', '100', '2019-01-20 20:37:49'),
(29, '000000000000000000000000000', 'june', '2023', '346', '2019-01-20 20:40:26'),
(30, '11111111111111111111111', 'january', '2016', '333', '2019-01-20 20:42:37'),
(31, '11111111111111111111111', 'december', '2018', '100', '2019-01-20 21:00:04'),
(32, '11111111111111111111111', 'january', '2016', '346', '2019-03-23 14:35:50');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `post_id` int(8) NOT NULL,
  `post_content` text NOT NULL,
  `post_date` datetime NOT NULL,
  `post_topic` int(8) NOT NULL,
  `post_by` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE `subscribers` (
  `id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `trn_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`id`, `email`, `trn_date`) VALUES
(1, 'chikaj35@yahoo.com', '2018-08-17 20:21:58'),
(2, 'chikaj35@yahoo.com', '2018-08-17 20:24:56'),
(3, 'chikaj35@yahoo.com', '2018-08-17 20:27:24'),
(4, 'chikaj35@yahoo.com', '2018-08-17 20:29:42'),
(5, 'chikaj35@yahoo.com', '2018-08-17 20:29:50'),
(6, 'chikaj35@yahoo.com', '2018-08-17 20:30:22'),
(7, 'chikaj35@yahoo.com', '2018-08-17 20:31:48'),
(8, 'chikaj35@yahoo.com', '2018-08-17 20:36:01'),
(9, 'chikaj35@yahoo.com', '2018-08-17 20:36:53'),
(10, 'metiehesther@gmail.com', '2018-08-17 20:38:34'),
(11, 'www@yahq.com', '2018-08-17 20:55:29'),
(12, 'www@yahq.com', '2018-08-17 20:55:29'),
(13, 'chikaj35@yahoo.com', '2018-08-17 20:55:33'),
(14, 'chikaj35@yahoo.com', '2018-08-17 20:55:33'),
(15, 'chikaj35@yahoo.com', '2018-08-17 20:55:40'),
(16, 'chikaj35@yahoo.com', '2018-08-17 20:55:40'),
(17, 'chikaj35@yahoo.com', '2018-08-17 20:55:51'),
(18, 'chikaj35@yahoo.com', '2018-08-17 20:55:51'),
(19, 'chikaj35@yahoo.com', '2018-08-17 20:56:05'),
(20, 'chikaj35@yahoo.com', '2018-08-17 20:56:18'),
(21, 'chikaj35@yahoo.com', '2018-08-17 20:57:00'),
(22, 'chikaj35@yahoo.com', '2018-08-17 20:59:58'),
(23, 'chikaj35@yahoo.com', '2018-08-17 21:00:03'),
(24, 'chikaj35@yahoo.com', '2018-08-17 21:00:18'),
(25, 'chikaj35@yahoo.com', '2018-08-17 21:00:35'),
(26, 'chikaj35@yahoo.com', '2018-08-17 21:00:48'),
(27, 'chikaj35@yahoo.com', '2018-08-17 21:01:24'),
(28, 'chikaj35@yahoo.com', '2018-08-17 21:01:29'),
(29, 'chikaj35@yahoo.com', '2018-08-17 22:21:00'),
(30, 'metiehesther@gmail.com', '2018-08-17 22:31:37'),
(31, 'chikaj35@yahoo.com', '2018-08-17 22:31:56'),
(32, 'metiehesther@gmail.com', '2018-08-17 22:32:11'),
(33, 'chikaj35@yahoo.com', '2018-08-17 22:32:22'),
(34, 'chikaj35@yahoo.com', '2018-08-17 22:33:07'),
(35, 'chikaj35@yahoo.com', '2018-08-17 22:33:14'),
(36, 'chikaj35@yahoo.com', '2018-08-17 23:21:19'),
(37, 'chikaj35@yahoo.com', '2018-08-17 23:21:33'),
(38, 'chikaj35@yahoo.com', '2018-08-17 23:24:17'),
(39, 'metiehesther@gmail.com', '2018-08-18 09:59:06'),
(40, 'chikaj35@yahoo.com', '2018-08-18 10:00:03'),
(41, 'chikaj35@yahoo.com', '2018-08-18 10:00:03'),
(42, 'chikaj35@yahoo.com', '2018-08-18 10:00:27'),
(43, 'chikaj35@yahoo.com', '2018-08-18 10:00:37'),
(44, 'metiehesther@gmail.com', '2018-08-18 10:00:59'),
(45, 'metiehesther@gmail.com', '2018-08-18 10:01:18'),
(46, 'metiehesther@gmail.com', '2018-08-18 10:01:18'),
(47, 'CHUUIII@Y.COM', '2018-08-18 10:32:59'),
(48, 'CHUUIII@Y.COM', '2018-08-18 10:33:14'),
(49, 'CHUUIII@Y.COM', '2018-08-18 10:36:40'),
(50, 'chikaj35@yahoo.com', '2018-08-27 15:53:52'),
(51, 'chikaj35@yahoo.com', '2018-08-27 15:54:08'),
(52, 'chikaj35@yahoo.com', '2018-08-27 15:54:25'),
(53, 'metiehesther@gmail.com', '2018-08-29 18:06:59'),
(54, 'chikaj35@yahoo.com', '2018-08-29 18:07:16'),
(55, 'chikhalifa23@yahoo.com', '2018-09-12 18:09:27'),
(56, 'lekangabs@outlook.com', '2018-10-19 11:39:43'),
(57, 'lekangabs@outlook.com', '2018-10-19 11:40:16'),
(58, 'lekangabs@outlook.com', '2018-10-19 11:40:21'),
(59, 'chikaj35@yahoo.com', '2019-03-05 18:34:45'),
(60, 'chikaj35@yahoo.com', '2019-03-25 22:23:35'),
(61, 'chikaj35@yahoo.com', '2019-03-25 22:23:43'),
(62, 'www@yahq.com', '2019-03-26 12:13:47'),
(63, 'www@yahq.com', '2019-03-26 12:16:02'),
(64, 'metiehesther@gmail.com', '2019-03-26 12:17:26'),
(65, 'chikaj35@yahoo.com', '2019-03-26 12:42:07'),
(66, 'metiehesther@gmail.com', '2019-03-27 14:20:43');

-- --------------------------------------------------------

--
-- Table structure for table `topics`
--

CREATE TABLE `topics` (
  `topic_id` int(8) NOT NULL,
  `topic_subject` varchar(255) NOT NULL,
  `topic_date` datetime NOT NULL,
  `topic_cat` int(8) NOT NULL,
  `topic_by` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tour2`
--

CREATE TABLE `tour2` (
  `id` int(11) NOT NULL,
  `fullname` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `states` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `apartment` varchar(50) NOT NULL,
  `rooms` varchar(50) NOT NULL,
  `date` varchar(50) NOT NULL,
  `trn_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tour2`
--

INSERT INTO `tour2` (`id`, `fullname`, `email`, `states`, `city`, `apartment`, `rooms`, `date`, `trn_date`) VALUES
(1, 'chika joseph', 'chikaj35@yahoo.com', '', 'ikeja', '', 'one selfcon', '23/07/2018', '2018-08-05 01:41:21'),
(2, 'chika joseph', 'chikaj35@yahoo.com', '', 'ikeja', '', 'one selfcon', '23/07/2018', '2018-08-05 01:41:54'),
(3, 'chika joseph', 'chikaj35@yahoo.com', '', 'ikeja', 'Normal Residenc', 'one selfcon', '23/07/2018', '2018-08-05 01:54:37'),
(4, 'chika joseph', 'chikaj35@yahoo.com', 'Lagos', 'ikeja', 'University Off-Campus', 'one selfcon', '23/07/2018', '2018-08-05 01:58:06'),
(5, 'chika joseph', 'chikaj35@yahoo.com', 'Lagos', 'YABA', 'University Off-Campus', 'two bedroom', '23/07/2018', '2018-08-09 12:38:47'),
(6, 'chika joseph', 'chikaj35@yahoo.com', 'Abia', 'YABA', 'Condo', 'two bedroom', '23/07/2018', '2018-08-16 02:28:14'),
(7, 'chika joseph', 'chikaj35@yahoo.com', 'Abia', 'YABA', 'Condo', 'two bedroom', '23/07/2018', '2018-08-16 02:28:15'),
(8, 'chika joseph', 'metiehesther@gmail.com', 'Lagos', 'ikeja', 'Normal Residence', 'one selfcon', '23/07/2018', '2018-08-17 23:19:47'),
(9, 'chika joseph', 'metiehesther@gmail.com', 'Lagos', 'ikeja', 'Normal Residence', 'one selfcon', '23/07/2018', '2018-08-17 23:19:47'),
(10, 'chika joseph', 'chikaj35@yahoo.com', 'Osun', 'ikeja', 'University Off-Campus', 'two bedroom', '23/07/2018', '2018-08-27 15:37:11'),
(11, 'chika joseph', 'chikaj35@yahoo.com', 'Delta', 'YABA', 'University Off-Campus', 'two bedroom', '23/07/2018', '2018-08-27 15:53:37'),
(12, 'fffffffffffffffffff', 'lekangabs@outlook.com', 'Abuja', 'yaba', 'University Off-Campus', 'two rooms selfcon', '222', '2018-09-07 07:50:25'),
(13, 'fffffffffffffffffff', 'chikhalifa23@yahoo.com', 'Delta', 'yaba', 'University Off-Campus', 'two rooms selfcon', '222', '2018-09-12 18:08:38'),
(14, 'chika', 'adkjf@gmail.com', 'Rivers', 'yaba', 'Rooftop/Penthouse', 'two rooms selfcon', '444444', '2018-09-14 15:42:54'),
(15, 'chika', 'osas@omoruyi.com', 'Lagos', 'Ikeja', 'University Off-Campus', 'two rooms selfcon', '444444', '2018-11-29 02:11:33'),
(16, 'femi nelson', 'metiehesther@gmail.com', 'Lagos', 'ikeja', 'Normal Residence', 'two bedroom', '23/07/2018', '2019-01-20 20:56:11'),
(17, 'chika joseph', 'metiehesther@gmail.com', 'Imo', 'YABA', 'Condo', 'one selfcon', '23/07/2018', '2019-01-20 22:44:12'),
(18, 'joshua nelson', 'ustt@uuu.com', 'Oyo', 'ayobo', 'University Off-Campus', 'two bedroom', '23/08/2019', '2019-03-05 11:43:17'),
(19, 'joshua nelson', 'ustt@uuu.com', 'Oyo', 'ayobo', 'University Off-Campus', 'two bedroom', '23/08/2019', '2019-03-05 11:43:17'),
(20, 'chika joseph', 'metiehesther@gmail.com', 'Lagos', 'ayobo', 'University Off-Campus', 'two bedroom', '23/08/2019', '2019-03-27 14:20:02');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(8) NOT NULL,
  `user_name` varchar(30) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_date` datetime NOT NULL,
  `user_level` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `trn_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `trn_date`) VALUES
(1, 'funke', 'tobi@yah.com', '6512bd43d9caa6e02c990b0a82652dca', '2018-06-20 13:08:49'),
(2, 'caryou', 'tobi@yah.com', '698d51a19d8a121ce581499d7b701668', '2018-06-20 13:09:44'),
(3, 'femi', 'femo@gm', '77963b7a931377ad4ab5ad6a9cd718aa', '2018-06-20 13:24:47'),
(4, 'femi', 'femo@gm', '77963b7a931377ad4ab5ad6a9cd718aa', '2018-06-20 13:24:47'),
(5, 'car', 'cccc@yyy', '0cc175b9c0f1b6a831c399e269772661', '2018-06-20 13:27:32'),
(6, 'Odyree', 'metiehesther@gmail.com', '37d153a06c79e99e4de5889dbe2e7c57', '2018-06-20 14:11:43'),
(7, 'black', 'cccc@yyy', 'bcbe3365e6ac95ea2c0343a2395834dd', '2018-06-20 15:01:30'),
(8, 'black', 'cccc@yyy', 'bcbe3365e6ac95ea2c0343a2395834dd', '2018-06-20 15:30:07'),
(9, 'h', 'a@h', '698d51a19d8a121ce581499d7b701668', '2018-06-20 15:30:49'),
(10, 'metiehesther@gmail.com', 'metiehesther@gmail.com', '37d153a06c79e99e4de5889dbe2e7c57', '2018-06-20 15:35:19'),
(11, 'metiehesther@gmail.com', 'cccc@yyy', '37d153a06c79e99e4de5889dbe2e7c57', '2018-06-21 13:23:10'),
(12, 'car', 'www@yah', '0cc175b9c0f1b6a831c399e269772661', '2018-06-21 14:35:54'),
(13, 'car', 'www@yahq', '0cc175b9c0f1b6a831c399e269772661', '2018-06-21 22:04:13'),
(14, 'Odyree', 'metiehesther@gmail.com', '37d153a06c79e99e4de5889dbe2e7c57', '2018-06-21 22:09:08'),
(15, 'car', 'chikaj35@yahoo.com', '0cc175b9c0f1b6a831c399e269772661', '2018-06-22 10:36:05'),
(16, 'fff', 'chikaj35@yahoo.com', '182be0c5cdcd5072bb1864cdee4d3d6e', '2018-06-27 21:04:10'),
(17, 'black', 'www@yah', 'ab86a1e1ef70dff97959067b723c5c24', '2018-06-27 21:05:21'),
(18, 'Odyree', 'cccc@yyy', '0745dc7435366c036e0b60723f7fa442', '2018-06-27 21:09:38'),
(19, 'chikaj35@yahoo.com', 'chikaj35@yahoo.com', '698d51a19d8a121ce581499d7b701668', '2018-07-03 20:30:05'),
(20, 'chikaj35@yahoo.com', 'metiehesther@gmail.com', '698d51a19d8a121ce581499d7b701668', '2018-07-03 20:31:24'),
(21, 'chikaj35@yahoo.com', 'ustt@uuu', '698d51a19d8a121ce581499d7b701668', '2018-07-03 20:35:15'),
(22, 'chika', 'chikhalifa23@yahoo.com', '3691308f2a4c2f6983f2880d32e29c84', '2018-07-26 22:10:04'),
(23, 'chika', 'chikhalifa23@yahoo.com', '11ddbaf3386aea1f2974eee984542152', '2018-07-26 22:52:29'),
(24, 'me', 'chikaj35@yahoo.com', '$2y$10$0hAWlXI6aghEngoMfV3oHutcgCpJmy65R2PpxkOx6KG', '2018-07-27 23:00:29'),
(25, 've', 'chikaj35@yahoo.com', '$2y$10$2/olNxclxnEwBdVyq83KJOEJ.yy37xEqUIy41BptBkk', '2018-07-27 23:01:24'),
(26, 'oo', 'chikaj35@yahoo.com', '$2y$10$253neAW1jCn8qGMnTnqmJuUK8T1MeApju7Xz7PuuPJ.', '2018-07-27 23:03:11'),
(27, 'e', 'chikaj35@yahoo.com', '$2y$10$hLfjrSttbv3BwCYREqQ72uLJgOIXEc4Ps1GhpGf7VIc', '2018-07-27 23:05:16'),
(28, 't', 'chikaj35@yahoo.com', '$2y$10$e.VT08Ej8izSQX9zZzT6LuWOn1YAukJbNg5vNvp79ZH', '2018-07-27 23:07:20'),
(29, 'me', 'chikaj35@yahoo.com', 'b59c67bf196a4758191e42f76670ceba', '2018-07-27 23:12:27'),
(30, 'me', 'chikaj35@yahoo.com', 'b59c67bf196a4758191e42f76670ceba', '2018-07-27 23:14:37'),
(31, 'me', 'chikaj@yahoo.com', '$2y$10$./6J6Qbs1KpRIglXsSp.IehgcRzVB7MWnSs80Fc0Ma4', '2018-07-28 03:23:51'),
(32, 'black', 'chikaj35@yahoo.com', 'ab86a1e1ef70dff97959067b723c5c24', '2018-07-28 04:28:41'),
(33, 'chikaj35@yahoo.com', 'chikaj35@yahoo.com', '698d51a19d8a121ce581499d7b701668', '2018-07-29 01:05:53'),
(34, 'chikaj35@yahoo.com', 'metiehesther@gmail.com', '698d51a19d8a121ce581499d7b701668', '2018-07-29 01:29:58'),
(35, 'car', 'chikaj35@yahoo.com', '0cc175b9c0f1b6a831c399e269772661', '2018-09-23 12:56:51'),
(36, 'metiehesther@gmail.com', 'chikaj35@yahoo.com', 'b59c67bf196a4758191e42f76670ceba', '2018-10-13 19:46:22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alpha2`
--
ALTER TABLE `alpha2`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`cat_id`),
  ADD UNIQUE KEY `cat_name_unique` (`cat_name`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact2`
--
ALTER TABLE `contact2`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `creditcard`
--
ALTER TABLE `creditcard`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`post_id`),
  ADD KEY `post_topic` (`post_topic`),
  ADD KEY `post_by` (`post_by`);

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `topics`
--
ALTER TABLE `topics`
  ADD PRIMARY KEY (`topic_id`),
  ADD KEY `topic_cat` (`topic_cat`),
  ADD KEY `topic_by` (`topic_by`);

--
-- Indexes for table `tour2`
--
ALTER TABLE `tour2`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_name_unique` (`user_name`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alpha2`
--
ALTER TABLE `alpha2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `cat_id` int(8) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contact2`
--
ALTER TABLE `contact2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `creditcard`
--
ALTER TABLE `creditcard`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `post_id` int(8) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subscribers`
--
ALTER TABLE `subscribers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `topics`
--
ALTER TABLE `topics`
  MODIFY `topic_id` int(8) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tour2`
--
ALTER TABLE `tour2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(8) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`post_topic`) REFERENCES `topics` (`topic_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `posts_ibfk_2` FOREIGN KEY (`post_topic`) REFERENCES `topics` (`topic_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `posts_ibfk_3` FOREIGN KEY (`post_by`) REFERENCES `user` (`user_id`) ON UPDATE CASCADE;

--
-- Constraints for table `topics`
--
ALTER TABLE `topics`
  ADD CONSTRAINT `topics_ibfk_1` FOREIGN KEY (`topic_cat`) REFERENCES `categories` (`cat_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `topics_ibfk_2` FOREIGN KEY (`topic_cat`) REFERENCES `categories` (`cat_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `topics_ibfk_3` FOREIGN KEY (`topic_by`) REFERENCES `user` (`user_id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
