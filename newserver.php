<?php
session_start();

// initializing variables
$username = "";
$email    = "";
$errors = array(); 

// connect to the database
$db = mysqli_connect('localhost', 'root', '', 'class2');

// REGISTER USER
if (isset($_POST['submit'])) {
  // receive all input values from the form
  $firstname = mysqli_real_escape_string($db, $_POST['firstname']);
$lastname = mysqli_real_escape_string($db, $_POST['lastname']);	
  $email = mysqli_real_escape_string($db, $_POST['email']);
  $psw_1 = mysqli_real_escape_string($db, $_POST['psw_1']);
  $psw_2 = mysqli_real_escape_string($db, $_POST['psw_2']);

  // form validation: ensure that the form is correctly filled ...
  // by adding (array_push()) corresponding error unto $errors array
  if (empty($firstname)) { array_push($errors, "Firstname is required"); }
if (empty($lastname)) { array_push($errors, "Lastname is required"); }	
  if (empty($email)) { array_push($errors, "Email is required"); }
  if (empty($psw_1)) { array_push($errors, "Password is required"); }
  if ($psw_1 != $psw_2) {
	array_push($errors, "The two passwords do not match");
  }

  // first check the database to make sure 
  // a user does not already exist with the same username and/or email
  $user_check_query = "SELECT * FROM alpha2 WHERE  email='$email' LIMIT 1";
  $result = mysqli_query($db, $user_check_query);
  $user = mysqli_fetch_assoc($result);
  
//  if ($user) { // if user exists
//    if ($user['username'] === $username) {
//      array_push($errors, "Username already exists");
//    }

    if ($user['email'] === $email) {
      array_push($errors, "email already exists");
    }
  

  // Finally, register user if there are no errors in the form
  if (count($errors) == 0) {
  	$password = md5($psw_1);//encrypt the password before saving in the database

  	$query = "INSERT INTO alpha2 (firstname,lastname, email, password) 
  			  VALUES('$firstname','$lastname' '$email', '$password')";
  	mysqli_query($db, $query);
  	$_SESSION['email'] = $email;
  	$_SESSION['success'] = "You are now logged in";
  	header('location: dashboard.php');
  }

}

//LOGIN USER
	
if (isset($_POST['submit'])) {
  $email = mysqli_real_escape_string($db, $_POST['email']);
  $psw_1 = mysqli_real_escape_string($db, $_POST['psw_1']);

  if (empty($email)) {
  	array_push($errors, "Email is required");
  }
  if (empty($psw_1)) {
  	array_push($errors, "Password is required");
  }

  if (count($errors) == 0) {
  	$password = md5($psw_1);
  	$query = "SELECT * FROM alpha2 WHERE email='$email' AND password='$password'";
  	$results = mysqli_query($db, $query);
  	if (mysqli_num_rows($results) == 1) {
  	  $_SESSION['email'] = $email;
  	  $_SESSION['success'] = "You are now logged in";
  	  header('location: dashboard.php');
  	}else {
  		array_push($errors, "Wrong username/password combination");
  	}
  }
}

?>