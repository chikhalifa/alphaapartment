
<?php
require('db.php');
?>
<?php 
  session_start(); 

  if (!isset($_SESSION['email'])) {
  	$_SESSION['msg'] = "You must log in first";
  	header('location: login.php');
  }
  if (isset($_GET['logout'])) {
  	session_destroy();
  	unset($_SESSION['email']);
  	header("location: login.php");
  }
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Dashboard - Secured Page</title>
   <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.css" />       
<!--<link rel="stylesheet" href="css/style.css" />-->
</head>
<body>
	<?php include'header.php';?>
	
<div class="container">
	<h2>Dashboard</h2>

<p><b>Only you can see this page.</b></p>
<!--<p><a href="index.php">Home</a></p>-->
 <button type="submit" class="btn btn-primary"  onclick="window.location.href='index.php'">Home</button>   
	<br>
	<br>
 <button type="submit" style="text-align:center" class="btn btn-info"  onclick="window.location.href='logout.php'">logout</button> 
          <br>
<!--<a href="logout.php">logout</a>-->
          
</div>
<br>
	<br>	<?php if (isset($_SESSION['success'])) : ?>
      <div class="error success" >
      	<h3>
          <?php 
          	echo $_SESSION['success']; 
          	unset($_SESSION['success']);
          ?>
      	</h3>
      </div>
  	<?php endif ?>

    <!-- logged in user information -->
    <?php  if (isset($_SESSION['email'])) : ?>
    	<p>Welcome <strong><?php echo $_SESSION['email']; ?></strong></p>
    	<p> <a href="index.php?logout='1'" style="color: red;">logout</a> </p>
    <?php endif ?>
	

<?php include'footer.php';?>
</body>
</html>